package fr.unice.mbds.app;

import java.io.IOException;
import java.util.Map;

import fr.unice.mbds.app.view.RouteEnum;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppFrame extends Application{

	private static final String TITLE  = "AppFrame";
	private static AppFrame singleton;
	
	private Stage primaryStage;
	private Map<String, Object> params;
	
	public AppFrame() {
		if(singleton!=null){
			return;
		}
		
		singleton = this;
	}
	 
	public static AppFrame getInstance(){
		if(singleton!=null){
			return singleton;
		}
		
		return new AppFrame();
	}
	
	
	public void changePage(RouteEnum route) throws IOException{
		Parent root = FXMLLoader.load(getClass().getResource(route.getPath()));
		primaryStage.getScene().setRoot(root);
	}
	
	public void changePage(RouteEnum route, Map<String, Object> params) throws IOException{
		this.params = params;
		changePage(route);
	}
	
	public Map<String, Object> getParams(){
		return params;
	}
	
	public Stage getPrimaryStage(){
		return primaryStage;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		primaryStage.setTitle(TITLE);
		
		
		Parent root = FXMLLoader.load(getClass().getResource(RouteEnum.LOGIN_PAGE.getPath()));
        Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}
		
	
	public static void main(String[] args) {
		launch(args);
	}

}
	
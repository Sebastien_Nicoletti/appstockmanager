package fr.unice.mbds.app.model;

import java.util.Observable;

import fr.unice.mbds.app.model.articles.Article;
import fr.unice.mbds.app.model.person.User;

public abstract class HomeModel extends Observable{
	public abstract User getUser();
	public abstract Article getArticleFav();
}

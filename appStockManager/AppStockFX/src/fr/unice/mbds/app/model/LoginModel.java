package fr.unice.mbds.app.model;

import java.util.Observable;

import fr.unice.mbds.app.model.person.User;

public abstract class LoginModel extends Observable{
	public abstract User connect(String login, String pwd);
}

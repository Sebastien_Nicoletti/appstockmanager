package fr.unice.mbds.app.model.person;

public enum Role {
	
	USER("USER"),
	ADMIN("ADMIN");
	
	private String name;
	
	private Role(String name) {
		this.name = name;
	}

	public String toString(){
		return name;
	}
}

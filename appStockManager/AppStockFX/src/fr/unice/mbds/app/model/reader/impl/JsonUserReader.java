package fr.unice.mbds.app.model.reader.impl;

import fr.unice.mbds.app.model.person.User;
import fr.unice.mbds.app.model.reader.UserReader;

public class JsonUserReader implements UserReader {
	
	private static final String LOGIN = "user";
	private static final String PWD = "user";

	@Override
	public User read(String login, String pwd) {
		if(!LOGIN.equals(login) || !PWD.equals(pwd)){
			return null;
		}
		
		User user = new User(login, login, login);
		return user;
	}

}

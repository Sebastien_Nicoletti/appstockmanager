package fr.unice.mbds.app.model.articles;

public class Article {
	
	private String name;
	private float price;
	
	public Article(String name, float price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public float getPrice() {
		return price;
	}
	
	@Override
	public Object clone(){
		return new Article(name, price);
	}
	
}

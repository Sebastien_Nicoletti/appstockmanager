package fr.unice.mbds.app.model.reader.impl;

import java.util.HashMap;

import fr.unice.mbds.app.model.person.User;
import fr.unice.mbds.app.model.reader.UserReader;

public class CSVUserReader implements UserReader {


	HashMap<String, String> users;

	@SuppressWarnings("resource")
	@Override
	public User read(String login, String pwd) {
		User user = null;
		try {

			System.out.println("------->TRY");
			java.io.InputStream is = getClass().getResourceAsStream("users.csv");
			System.out.println(getClass().getResource("users.csv"));
			java.util.Scanner s = new java.util.Scanner(is, "UTF-8").useDelimiter("\r\n");
			//  s.next();//On ignore l'entete du CSV
			System.out.println("->>>>>>#########Amptempting to feed the database".toUpperCase());
			while (s.hasNext()) {

				String[] etabStrings = s.next().split(";");

				if(etabStrings[0].equals(login) && etabStrings[1].equals(pwd)){
					user = new User(etabStrings[0], etabStrings[2], etabStrings[3]);
				}
			}
			s.close();

		} catch (Exception e) {
			e.printStackTrace();

		}
		
		return user;
	}

}

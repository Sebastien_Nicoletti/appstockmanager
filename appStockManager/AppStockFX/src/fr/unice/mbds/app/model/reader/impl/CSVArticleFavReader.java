package fr.unice.mbds.app.model.reader.impl;

import fr.unice.mbds.app.model.articles.Article;
import fr.unice.mbds.app.model.reader.ArticleFavReader;

public class CSVArticleFavReader implements ArticleFavReader{

	@SuppressWarnings("resource")
	@Override
	public Article reader() {
		Article article = null;
		try {

			System.out.println("------->TRY");
			java.io.InputStream is = getClass().getResourceAsStream("articles.csv");
			System.out.println( getClass().getResource("articles.csv").toString());
			java.util.Scanner s = new java.util.Scanner(is, "UTF-8").useDelimiter("\r\n");
			//  s.next();//On ignore l'entete du CSV
			System.out.println("->>>>>>#########Amptempting to feed the database".toUpperCase());
			if(s.hasNext()) {
				String[] etabStrings = s.next().split(";");
				article = new Article(etabStrings[0], Float.parseFloat(etabStrings[1]));
			}
			s.close();

		} catch (Exception e) {
			e.printStackTrace();

		}
		
		return article;
	}

}

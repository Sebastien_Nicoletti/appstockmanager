package fr.unice.mbds.app.model.reader.impl;

import fr.unice.mbds.app.model.articles.Article;
import fr.unice.mbds.app.model.reader.ArticleFavReader;

public class JsonArticleFavReader implements ArticleFavReader{

	@Override
	public Article reader() {	
		return new Article("Article 1", 5f);
	}

}

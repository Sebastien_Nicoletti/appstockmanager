package fr.unice.mbds.app.model.person;

import fr.unice.mbds.app.model.articles.Panier;

public class User {
	
	private String login;
	private String firstname;
	private String lastname;
	private Panier panier;
	private Role role;
	
	public User(String login, String lastname, String firstname ){
		this.login = login;
		this.lastname = lastname;
		this.firstname = firstname;
		panier = new Panier();
		role = Role.USER;
		System.out.println(lastname + " " + firstname);
	}
	
	public User(String login, String lastname, String firstname, Panier panier) {
		this.login = login;
		this.lastname = lastname;
		this.firstname = firstname;
		this.panier = panier;
		role = Role.USER;
	}

	public String getLogin() {
		return login;
	}

	public Panier getPanier() {
		return panier;
	}

	public Role getRole() {
		return role;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}
	
}

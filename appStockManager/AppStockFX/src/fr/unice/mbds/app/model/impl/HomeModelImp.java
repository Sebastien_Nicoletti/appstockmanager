package fr.unice.mbds.app.model.impl;

import fr.unice.mbds.app.model.HomeModel;
import fr.unice.mbds.app.model.articles.Article;
import fr.unice.mbds.app.model.person.User;
import fr.unice.mbds.app.model.reader.ArticleFavReader;
import fr.unice.mbds.app.model.reader.impl.CSVArticleFavReader;

public class HomeModelImp extends HomeModel{
	
	private User user;
	private ArticleFavReader articleFavReader;
	
	public HomeModelImp(User user) {
		this.user = user;
		this.articleFavReader = new CSVArticleFavReader();
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public Article getArticleFav() {
		return articleFavReader.reader();
	}
}

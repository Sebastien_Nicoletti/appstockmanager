package fr.unice.mbds.app.model.reader;

import fr.unice.mbds.app.model.articles.Article;

public interface ArticleFavReader {
	public Article	 reader();

}

package fr.unice.mbds.app.view;

public enum RouteEnum {

	LOGIN_PAGE("./view/LoginView.fxml"),
	HOME_PAGE("./view/HomeView.fxml");
	
	private String path;
	
	private RouteEnum(String path) {
		this.path = path;
	}
	
	
	public String getPath(){
		return path;
	}

}

package fr.unice.mbds.app.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.unice.mbds.app.AppFrame;
import fr.unice.mbds.app.model.HomeModel;
import fr.unice.mbds.app.model.articles.Article;
import fr.unice.mbds.app.model.impl.HomeModelImp;
import fr.unice.mbds.app.model.person.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import fr.unice.mbds.app.view.*;;


public class HomeController{
	
	private static final int SIZE_SCREEN = 500;
	
	 @FXML private Button previousButton;
	 @FXML private Label welcomeText;
	 @FXML private Label favProduct;
	 @FXML private TextField amountInput;
	 @FXML private Button pannierButton;
	
	//Model
	private HomeModel model;
	private User user;
	private Article articleFav;
	
	@FXML
	private void initialize(){
		//Model
		Map<String, Object> params = AppFrame.getInstance().getParams();
		user = (User) params.get("user");	
		model = new HomeModelImp(user);
		articleFav = model.getArticleFav();
		
		//update the view 
		Stage primaryStage = AppFrame.getInstance().getPrimaryStage();
		primaryStage.setResizable(true);
		primaryStage.setWidth(SIZE_SCREEN);
		primaryStage.setHeight(SIZE_SCREEN);
		
		//Change text
		welcomeText.setText("Bonjour, " + user.getFirstname() + " " + user.getLastname());
		favProduct.setText("Le produit du jour est le \"" + articleFav.getName() + "\" au prix HT de " + articleFav.getPrice() + "�");
	}
	
	private void close(){
		try {
			AppFrame.getInstance().changePage(RouteEnum.LOGIN_PAGE,new HashMap<>());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	@FXML
	private void handleButtonPrevious(ActionEvent event) {
		close();
	}
	
	@FXML
	private void handleButtonAddProduct(ActionEvent event) {
		try{
			int amount = Integer.parseInt(amountInput.getText());
			
			for(int i = 0; i < amount; i++){
				user.getPanier().addArticle((Article)articleFav.clone());
			}
			
			close();
		}catch (NumberFormatException e) {
			amountInput.setText("");
		}
	}
}

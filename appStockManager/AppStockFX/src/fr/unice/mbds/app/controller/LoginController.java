package fr.unice.mbds.app.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.unice.mbds.app.AppFrame;
import fr.unice.mbds.app.model.LoginModel;
import fr.unice.mbds.app.model.impl.LoginModelImpl;
import fr.unice.mbds.app.model.person.User;
import fr.unice.mbds.app.view.RouteEnum;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginController{
	
	private static final int SIZE_HEIGHT_SCREEN = 400;
	private static final int SIZE_WIDTH_SCREEN = 500;

	//Views	
	@FXML private TextField loginField;
	@FXML private PasswordField pwdField;
	@FXML private Label errorLabel;
	@FXML private Button connectButton;

	//models
	private LoginModel model;


	@FXML
	public void initialize() {
		model = new LoginModelImpl();
		errorLabel.setText("");
		
		//updaye view
		Stage primaryStage = AppFrame.getInstance().getPrimaryStage();
		primaryStage.setResizable(false);
		primaryStage.setWidth(SIZE_WIDTH_SCREEN);
		primaryStage.setHeight(SIZE_HEIGHT_SCREEN);
	}



	@FXML private void handleButtonConnection(ActionEvent event) {
		try {
			connection();
		} catch (IOException e) {
			e.printStackTrace();
			errorLabel.setText("Error: Load page connection");
		}
	}

	private void connection()throws IOException{
		String login = loginField.getText();	
		String pwd = pwdField.getText();

		User user = model.connect(login,pwd);

		if(user != null){
			Map<String, Object> params = new HashMap<>();
			params.put("user", user);
			AppFrame.getInstance().changePage(RouteEnum.HOME_PAGE, params);
		}
		else{
			errorLabel.setText("Login/Password incorrect...");
		}
	}

}

package fr.unice.mbds.app;

import java.util.HashMap;
import java.util.Map;

import fr.unice.mbds.app.controller.impl.LoginController;

public class Main {
	public static void main(String[] args) {
		AppFrame appFrame = new AppFrame("App Stock", 500, 500);
		Map<String, Object> data = new HashMap<>();
		new LoginController().init(appFrame, data);
	}
}

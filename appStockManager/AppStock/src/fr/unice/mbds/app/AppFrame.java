package fr.unice.mbds.app;

import javax.swing.JFrame;

public class AppFrame extends JFrame{
	
	private static final long serialVersionUID = 1L;
	
	public AppFrame(String title, int width, int height) {
		setTitle(title);
		setSize(width, height);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

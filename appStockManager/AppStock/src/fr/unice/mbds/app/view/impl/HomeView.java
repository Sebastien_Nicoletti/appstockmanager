package fr.unice.mbds.app.view.impl;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;

import fr.unice.mbds.app.model.articles.Article;
import fr.unice.mbds.app.view.AppView;

public class HomeView extends AppView{

	private static final long serialVersionUID = 1L;
	
	String[] columnNames = {"Produit", "Prix"};
	
	private JButton closeButton;
	
	private JTable table;
	
	public HomeView() {
		closeButton = new JButton("Precedent");
		closeButton.setActionCommand("ACTION_CLOSE");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void init(Map<String, Object> data) {
		String username = (String) data.get("username");
		ActionListener al = (ActionListener) data.get("action_listener");
		
		List<Article> articles = (List<Article>) data.get("articles");
		createView(username, articles);
		
		closeButton.addActionListener(al);
	}
	
	private void createView(String username, List<Article> articles){
		setLayout(new BorderLayout());
		
		//Header	
		add(new JLabel("Bonjour, " + username), BorderLayout.NORTH);
		
		//Articles
		String[][] data = new String[articles.size()][columnNames.length];
		for(int i = 0; i < articles.size(); i++){
			Article a = articles.get(i);
			data[i][0] = a.getName();
			data[i][1] = a.getPrice() + "";
		}
		table = new JTable(data, columnNames);
		table.setEnabled(false);
		add(table, BorderLayout.CENTER);
		
		//Close button
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		add(buttonPanel, BorderLayout.SOUTH);
		buttonPanel.add(closeButton);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map<String, Object> getData() {
		return null;
	}

}

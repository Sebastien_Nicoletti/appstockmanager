package fr.unice.mbds.app.view.impl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import fr.unice.mbds.app.view.AppView;

public class LoginView extends AppView	{
	
	private static final long serialVersionUID = 1L;
	
	private JTextField loginField;
	private JPasswordField passwordField;
	
	private JButton connexionButton;
	private JButton closeButton;
	
	private JLabel errorLabel;
	private JLabel loginLabel;
	private JLabel passwordLabel;
	
	public LoginView() {
		loginField = new JTextField();
		passwordField = new JPasswordField();
		errorLabel = new JLabel();
		errorLabel.setForeground(Color.red);
		connexionButton = new JButton("Connexion");
		connexionButton.setActionCommand("ACTION_AUTHENTICATION");
		closeButton = new JButton("Fermer");
		closeButton.setActionCommand("ACTION_CLOSE");
		createView();
	}
	
	private void createView(){
		
setLayout(new BorderLayout());
		
JPanel panel = new JPanel();

add(panel, BorderLayout.CENTER);

		panel.setLayout(null);

		JLabel userLabel = new JLabel("Login");
		userLabel.setBounds(10, 50, 80, 25);
		panel.add(userLabel);

		loginField = new JTextField(20);
		loginField.setBounds(100, 50, 180, 25);
		panel.add(loginField);

		JLabel passwordLabel = new JLabel("Password");
		passwordLabel.setBounds(10, 80, 80, 25);
		panel.add(passwordLabel);

		passwordField = new JPasswordField(20);
		passwordField.setBounds(100, 80, 180, 25);
		panel.add(passwordField);

		
		errorLabel.setBounds(10, 10, 500, 25);
		panel.add(errorLabel);
		
		connexionButton.setBounds(10, 120, 100, 25);
		panel.add(connexionButton);
		
		closeButton.setBounds(180, 120, 100, 25);
		panel.add(closeButton);
		 
	
	}

	@Override
	public void init(Map<String, Object> data) {
		if(data.containsKey("action_listener")){
			ActionListener al = (ActionListener) data.get("action_listener");
			connexionButton.addActionListener(al);
			closeButton.addActionListener(al);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void update(Observable obs, Object obj) {
		Map<String, String> data = (Map<String, String>)obj;	
		errorLabel.setText(data.get("error"));
	}

	@Override
	public Map<String, Object> getData() {
		Map<String, Object> data = new HashMap<>();
		data.put("login", loginField.getText().toString());
		data.put("pwd", new String(passwordField.getPassword()));
		return data;
	}

}

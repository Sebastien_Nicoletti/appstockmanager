package fr.unice.mbds.app.view;

import java.util.Map;
import java.util.Observer;

import javax.swing.JPanel;

public abstract class AppView extends JPanel implements Observer{

	private static final long serialVersionUID = 1L;
	
	public abstract void init(Map<String, Object> data);
	public abstract Map<String, Object> getData();
}

package fr.unice.mbds.app.model.impl;

import java.util.HashMap;
import java.util.Map;

import fr.unice.mbds.app.model.LoginModel;
import fr.unice.mbds.app.model.person.User;
import fr.unice.mbds.app.model.reader.UserReader;
import fr.unice.mbds.app.model.reader.impl.CSVUserReader;

public class LoginModelImpl extends LoginModel {
	
	private UserReader userReader;
	
	public LoginModelImpl() {
		userReader = new CSVUserReader();
	}

	@Override
	public User connect(String login, String pwd) {
		User user = userReader.read(login, pwd);
		
		if( user == null ){
			Map<String, Object> errData = new HashMap<>();
			errData.put("error", "Login/Password pas valide...");
			setChanged();
			notifyObservers(errData);
		}

		return user;
	}
}

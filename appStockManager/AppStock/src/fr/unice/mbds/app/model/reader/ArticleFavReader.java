package fr.unice.mbds.app.model.reader;

import java.util.List;

import fr.unice.mbds.app.model.articles.Article;

public interface ArticleFavReader {
	public List<Article> reader();

}

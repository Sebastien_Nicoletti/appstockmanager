package fr.unice.mbds.app.model.person;

import fr.unice.mbds.app.model.articles.Panier;

public class User {
	
	private String login;
	private Panier panier;
	private Role role;
	
	public User(String login){
		this.login = login;
		panier = new Panier();
		role = Role.USER;
	}
	
	public User(String login, Panier panier) {
		this.login = login;
		this.panier = panier;
		role = Role.USER;
	}

	public String getLogin() {
		return login;
	}

	public Panier getPanier() {
		return panier;
	}

	public Role getRole() {
		return role;
	}
	
	
	
	
}

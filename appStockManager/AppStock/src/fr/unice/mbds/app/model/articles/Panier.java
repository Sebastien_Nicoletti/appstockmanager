package fr.unice.mbds.app.model.articles;

import java.util.ArrayList;
import java.util.List;

public class Panier {
	
	private List<Article> articles;
	
	public Panier() {
		articles = new ArrayList<>();
	}
	
	public Article[] getArticles(){
		return articles.toArray(new Article[articles.size()]);	
	}
	
	public boolean addArticle(Article article){
		return articles.add(article);
	}
	
	public boolean removeArticle(Article article){
		return articles.remove(article);
	}
	
	public float prices(){
		float price = 0;
		
		for(Article a : articles){
			price += a.getPrice();
		}
		
		return price;
	}

}

package fr.unice.mbds.app.model.reader.impl;

import java.util.ArrayList;
import java.util.List;

import org.omg.CORBA.portable.InputStream;

import fr.unice.mbds.app.model.articles.Article;
import fr.unice.mbds.app.model.reader.ArticleFavReader;

public class CSVArticleFavReader implements ArticleFavReader{

	@Override
	public List<Article> reader() {
		
		List<Article> list = new ArrayList<>();
		
		
		
		try {

            System.out.println("------->TRY");
            java.io.InputStream is = getClass().getResourceAsStream("articles.csv");
           System.out.println( getClass().getResource("articles.csv").toString());
            java.util.Scanner s = new java.util.Scanner(is, "UTF-8").useDelimiter("\r\n");
          //  s.next();//On ignore l'entete du CSV
            System.out.println("->>>>>>#########Amptempting to feed the database".toUpperCase());
            while (s.hasNext()) {
                String[] etabStrings = s.next().split(";");
        		list.add(new Article(etabStrings[0], Float.parseFloat(etabStrings[1])));
            }
            s.close();

        } catch (Exception e) {
            e.printStackTrace();

        }
    

		
		
		
		return list;
	}

}

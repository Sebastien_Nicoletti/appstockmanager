package fr.unice.mbds.app.model.reader;

import fr.unice.mbds.app.model.person.User;

public interface UserReader {
	
	public User read(String login, String pwd);

}

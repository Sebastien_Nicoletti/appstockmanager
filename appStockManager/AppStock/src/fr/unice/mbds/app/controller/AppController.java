package fr.unice.mbds.app.controller;

import java.util.Map;

import fr.unice.mbds.app.AppFrame;

public interface AppController {
	public void init(AppFrame appFrame, Map<String, Object> data);
	public void stop();	
}

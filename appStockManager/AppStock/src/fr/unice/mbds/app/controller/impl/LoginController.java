package fr.unice.mbds.app.controller.impl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import fr.unice.mbds.app.AppFrame;
import fr.unice.mbds.app.controller.AppController;
import fr.unice.mbds.app.model.LoginModel;
import fr.unice.mbds.app.model.impl.LoginModelImpl;
import fr.unice.mbds.app.model.person.User;
import fr.unice.mbds.app.view.AppView;
import fr.unice.mbds.app.view.impl.LoginView;

public class LoginController implements AppController, ActionListener{

	private static final String ACTION_AUTHENTICATION = "ACTION_AUTHENTICATION";
	private static final String ACTION_CLOSE = "ACTION_CLOSE";

	//view
	private AppFrame appframe;
	private AppView loginView;

	//models
	private LoginModel model;
	private Map<String, Object> config;

	public LoginController() {
		loginView = new LoginView();	
	}

	@Override
	public void init(AppFrame appFrame, Map<String, Object> config) {
		this.appframe = appFrame;
		this.config = config;
		this.model = new LoginModelImpl();

		//init observer/observable
		this.model.addObserver(loginView);

		//init view
		Map<String, Object> data = new HashMap<>();
		data.put("action_listener", this);
		loginView.init(data);

		//init frame
		appFrame.setContentPane(loginView);
		appFrame.setSize(300, 200);
		appFrame.setVisible(true);
	}

	@Override
	public void stop() {
		System.exit(0);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if( ACTION_AUTHENTICATION.equals(e.getActionCommand()) ){
			connection();
		}else if (ACTION_CLOSE.equals(e.getActionCommand())){
			stop();
		}
	}

	private void connection(){
		Map<String, Object> data = loginView.getData();
		String login = data.get("login").toString();
		String pwd = data.get("pwd").toString();
		User user = model.connect(login,pwd);

		if(user != null){
			config.put("user", user);
			new HomeController().init(appframe, config);
		}
	}

}

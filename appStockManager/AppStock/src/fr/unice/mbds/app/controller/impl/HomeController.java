package fr.unice.mbds.app.controller.impl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import fr.unice.mbds.app.AppFrame;
import fr.unice.mbds.app.controller.AppController;
import fr.unice.mbds.app.model.HomeModel;
import fr.unice.mbds.app.model.impl.HomeModelImp;
import fr.unice.mbds.app.model.person.User;
import fr.unice.mbds.app.view.AppView;
import fr.unice.mbds.app.view.impl.HomeView;

public class HomeController implements AppController, ActionListener{
	
	private static final String ACTION_CLOSE = "ACTION_CLOSE";
	
	//View
	private AppFrame appFrame;
	private AppView homeView;
	
	//Model
	private HomeModel model;
	private Map<String, Object> config;
	
	public HomeController() {
		homeView = new HomeView();
	}

	@Override
	public void init(AppFrame appFrame, Map<String, Object> config) {
		this.appFrame = appFrame;
		this.config = config;
		
		//init model
		User user = (User)config.get("user");
		model = new HomeModelImp(user);
		
		//init view
		Map<String, Object> dataView = new HashMap<>();
		dataView.put("action_listener", this);
		dataView.put("username", user.getLogin());
		dataView.put("articles", model.getArticlesFav());
		homeView.init(dataView);
		
		//init observer / observable
		model.addObserver(homeView);		
		
		//Start view
		appFrame.setContentPane(homeView);
		appFrame.setSize(500,500);
		appFrame.validate();
	}

	@Override	
	public void stop() {
		config.remove("user");
		new LoginController().init(appFrame, config);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(ACTION_CLOSE.equals(e.getActionCommand())){
			stop();
		}
	}
	


}
